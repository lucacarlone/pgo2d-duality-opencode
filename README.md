# README #

### INTRODUCTION ###

* This matlab code creates a 2D (random) pose graph optimization (PGO) problem, and
* 1) iteratively solves the primal problem using gauss newton (GN)
* 2) solves the dual (SDP)
* 3) computes the duality gap

### REQUIREMENTS ###

* the code requires CVX (http://cvxr.com/cvx/) to perform the optimization

### RUNNING THE CODE ###

* execute "main.m" using Matlab

### CITING THIS WORK ###

```
@inproceedings{Carlone15icra,
	Author = {L. Carlone and F. Dellaert},
	Booktitle = icra,
	Title = {Duality-based Verification Techniques for {2D SLAM}},
	Pages = {4589--4596},
	Year = 2015
}
```

```
@article{Carlone15tro,
	Author = {L. Carlone and G. Calafiore and C. Tommolillo and F. Dellaert},
	Journal = {IEEE Trans. on Robotics, Conditionally accepted},
	Title = {Planar Pose Graph Optimization: Duality, Optimal Solutions, and Verification},
	Year = 2015
}
```

```
@inproceedings{Carlone15rssws2D,
  title={Pose Graph Optimization in the Complex Domain: Lagrangian Duality and Optimal Solutions},
  author={L. Carlone and G. Calafiore and F. Dellaert},
  booktitle={RSS workshop "Reviewing the review process", 
  available online at http://openreview.informatik.uni-freiburg.de/node/41},
  year={2015}
}
```

### CONTACTING THE AUTHOR ###
```
% =============================================================================
Luca Carlone
email: lcarlone@mit.edu
website: lucacarlone.com
Massachusetts Institute of Technology
% =============================================================================
```
