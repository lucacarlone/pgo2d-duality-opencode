function [edges, thGT, posesGT, posesEst] = buildRandomMeasurements2D(edges_id, isUniform, sigmaR, sigmaT, style)
% edges: standard matrix to encode 2D measurements
% thEstGT: vector with nrNodes entries with GT node orientation
% posesGT: nrNodes X 3 matrix with GT node positions by rows
% Luca Carlone
% Georgia Institute of Technology
% Dec 2014

[nrNodes,m,l,n] = sizeDataset(edges_id);

if nargin < 4
  error('check API for buildRandomMeasurements2D')
  sigmaT = 0.01;
  sigmaR = 0.01;
  isUniform = 1;
  style = 'random'; % 'circle'
end

if nargin == 4
  style = 'random'; % 'circle'
end

cartesianBounds = 10;
if isUniform
  cprintf('_cyan','Uniform noise on pose measurements\n')
end

%% create poses
posesGT = zeros(nrNodes,3);
for i=1:nrNodes
    if strcmp(style,'random')==1
        xy = cartesianBounds * rand(1,2);
    elseif strcmp(style,'circle')==1
        polar_i = (i-1) * (2 * pi)/(nrNodes) - pi/2; % starts at the bottom
        xy = cartesianBounds/2 * [cos(polar_i) sin(polar_i)];
    else
        error('unknown style')
    end
    th = 2*pi * (rand-0.5); % in [-pi,pi]
    posesGT(i,:) = [xy th];
end

%% create edges
edges = [];
for k=1:m
    i = edges_id(k,1);
    j = edges_id(k,2);
    p_i = posesGT(i,:)';
    p_j = posesGT(j,:)';
    deltaPoseNoisy = poseSubNoisy(p_j , p_i, sigmaT, sigmaR, isUniform);
    edges(end+1,1:11) = [i j deltaPoseNoisy' 1 0 0 1 0 1];
end

%% Save *true* angles
thGT = posesGT(:,3);

%% Noisy initial guess
posesEst = odometryFromEdges(edges,nrNodes); % composing measurements along ST
