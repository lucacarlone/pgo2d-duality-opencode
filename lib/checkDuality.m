function [nrFail, nrMultiZero, doSave] = checkDuality(pstar,dstar,D_QtildeLambda,nrFail,nrMultiZero)

doSave = 0;
if norm(pstar)>1e-8 && norm(pstar - dstar)/pstar > 1e-2
    pstar, dstar, warning('Duality failed')
    nrFail = nrFail+1;
end
zeroMultiplicity = length(find(diag(D_QtildeLambda) < 1e-7 ));
if zeroMultiplicity ~= 1 % zero eigenvalue is not simple
    zeroMultiplicity, D_QtildeLambda, pstar, dstar, warning('Non simple eigenvalue at zero')
    nrMultiZero = nrMultiZero+1;
    doSave = 1;
end