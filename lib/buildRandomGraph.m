function [edges, thGT, posesGT, posesEst] = buildRandomGraph(nrNodes, isUniform, sigmaR, sigmaT, pLC, scale)
% Creates random graph, with a pose associated to nodes and relative pose
% measurements associated to eges
% 
% INPUTS:
% - nrNodes: number of nodes in the graph
% - isUniform: use uniform, rather than gaussian noise
% - sigmaR: std of rotation noise
% - sigmaT: std of translation noise
% - pLC: probability of creating a loop closure between 2 nodes
% - scale: multiplies the translation part by a scale factor
% OUTPUTS:
% - edges: m x 11 matrix describing edges (in g2o format): each row, describe edge (i,j) 
%          and is in the form [i j pij Ixx Ixy Ixth Iyy Ixth Ithth] where i
%          and j are the node id, pij = [x y th] is the 2D measured relative pose and
%          I describe the entries of the information matrix describing
%          measurement noise
% - thGT: ground truth orientation of the nodes generating the measurements
% - posesGT: ground truth poses of the nodes generating the measurements
% - posesEst: pose estimate (odometry) obtained  by concatenating measurements
%
% Author: Luca Carlone
% Date: 2015-1-1
% Institution: Georgia Institute of Technology
%

if nargin < 2
  isUniform = 0; % we use Normally distributed noise
  sigmaT = 0.1;
  sigmaR = 0.1;
  pLC = 0.3;
  scale = 1;
end

%% We start by creating a spanning *path*
isLineGraph = 1;
[edges, thGT, posesGT, posesEst] = buildRandomST(nrNodes, isLineGraph, isUniform, sigmaR, sigmaT, scale); 

%% add random loop closures
isSimpleGraph = 1;
n = size(edges,1); % spanning tree has nrNodes-1 edges
nrNodes = n+1;
%
for i=1:nrNodes
  otherNodes = setdiff(randperm(nrNodes),i); % order randomly other nodes, not to bias loop closure selection
  for j = otherNodes
    if isSimpleGraph && ... % only add if the edge is not there
        length(find(edges(:,1)==i & edges(:,2)==j))==0 && ...
        length(find(edges(:,2)==i & edges(:,1)==j))==0
      
      % add loop with some probability
      if rand < pLC
        p_i = posesGT(i,:)';
        p_j = posesGT(j,:)';
        deltaPoseNoisy = poseSubNoisy(p_j , p_i, sigmaT, sigmaR, isUniform);
        edges(end+1,1:11) = [i j deltaPoseNoisy' 1/sigmaT^2 0 0 1/sigmaT^2 0 1/sigmaR^2];
      end
    end
  end
end