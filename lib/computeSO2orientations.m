function [th_round, th_star, theta_set, delta_round, gamma_round, C, Cl, chords] = computeSO2orientations(A, At, edges, P_delta_inv, useMOLE2D, CBM, ST, MST, C1, C2)
%
% Regularization and computation of planar nodes orientations according to
% the paper "L. Carlone and A. Censi, From Angular Manifolds to the 
% Integer Lattice: Guaranteed Orientation Estimation with Application to 
% Pose Graph Optimization, IEEE Trans. on Robotics, (30)2: 475-492, 2014."
% 
% If useMOLE2D = 0, it will only do the regularization using the odometric
% spanning tree. It assumes that the first nrNodes-1 edges are (1,2),
% (2,3), .., (nrNodes-1,nrNodes), and then there are the loop closures
% 
% Author: Luca Carlone
% Date: Jan 2013

delta = edges(:,5);
[~,m,l,n] = sizeDataset(edges);
chords = [n+1:m];

showTime = 0;
plotFlag = 0;

%% Compute the estimator gammahat and the corresponding covariance
if ~useMOLE2D
  disp('CBM-less regularization - assumes ordered edges')
  mu_gamma_init = zeros(l,1); 
  for i=1:l   % for each loop
    mu_gamma_init(i) = delta(n+i);
    if edges(n+i,1)<edges(n+i,2)
      mu_gamma_init(i) = mu_gamma_init(i) - sum( delta(edges(n+i,1):edges(n+i,2)-1) ); % edges in the spanning tree
    else
      mu_gamma_init(i) = mu_gamma_init(i) + sum( delta(edges(n+i,2):edges(n+i,1)-1) ); % edges in the spanning tree
    end
  end
  gamma_round = round(1/(2*pi) * mu_gamma_init);
  %warning('added wraparound error')
  %gamma_round(120) = gamma_round(120)+1 % 190
  disp('computed gamma_round')
  [th_round, delta_round] = theta_given_gamma(gamma_round, A, At, delta, P_delta_inv, chords);
  th_star = []; theta_set = [];
  disp('computed delta_round')
  
  if nargout >= 6 % we want the cycle basis as output
    disp('User required to compute the cycle matrix (fundamental, odometric)')
    tic
    [C, Cl] = createFCBO(edges(:,1:2));
    toc
    % debug
    mu_gamma_init_2 = C*delta;
    if norm(mu_gamma_init - mu_gamma_init_2) > 1e-4
      error('Regularization does not match results from cycle basis')
    end
  end
end

%% MOLE2D ESTIMATION
if useMOLE2D
  
  %% BUILD THE CYCLE BASIS MATRIX
  %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  switch  CBM
    case 0
      disp('WE ARE USING THE CBM BUILT ON THE ODOMETRIC SPANNING TREE')
      [C, Cl] = createFCBO(edges(:,1:2));
    case 1
      disp('WE ARE USING THE MINIMUM CYCLE BASIS')
      if rank(C1(:,chords)) < l
        error('C needs a different chord selection!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        chords = select_chords(C1);
      end
      C = sparse(C1);
      Cl = C(:,chords);
      %e = inv(Cl) * (C * delta);
    case 2
      disp('WE ARE USING THE APPROXIMATE MINIMUM CYCLE BASIS')
      C = sparse(C2);
      Cl = C(:,chords);
      %e = inv(Cl) * (C * delta);
    case 3
      error('CBM=3 is deprecated (CBM BUILT ON THE MINIMUM SPANNING TREE)')
      disp('WE ARE USING THE CBM BUILT ON THE MINIMUM SPANNING TREE')
      find_C_mST
      Cl = C(:,chords);
    otherwise
      error('Incorrect choice of the cycle basis matrix')
  end
  
  %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  if plotFlag == 1
    figure
    spy(C)
    
    disp('Dimension of the cycle basis')
    disp(sum(sum(abs(C))))
  end
  
  disp('Longest cycle')
  disp(max(sum( abs(C) , 2 )))
  
  tic
  mu_gamma_init = 1/(2*pi) * C*delta;
  P_gamma_init = 1/(4*pi^2) * C*inv(P_delta_inv)*C';
  time_gammahat = toc;
  
  % check
  disp('max cycle error')
  max(abs( mu_gamma_init - round(mu_gamma_init )))

  %% Apply the INTEGER-SCREENING algorithm
  alfa=0.99; %confidence level for the choice of the correction factors
  stringa = sprintf('Overall Confidence level alpha: %f',alfa);
  disp(stringa)
  eta =  alfa^(1/l);
  stringa = sprintf('Confidence level eta: %f',eta);
  disp(stringa)
  %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  disp('VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV')
  disp('INTEGER SCREENING')
  tic
  [mu_gamma, mu_gamS, P_gamS, gamma_id, TOT_comb, counter_inconsistent, v, alternatives, GAMMA, GAMMA_gamS] ...
    = INTEGER_SCREENING(mu_gamma_init, P_gamma_init, eta, plotFlag);
  time_int_screening = toc;
  TOT_comb
  disp('AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
  %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  COMB_MAX = 2000;
  if TOT_comb==1
    disp('A single combination fits the data - good shot')
  end
  
  %% compute the theta for each gamma in Gamma
  if TOT_comb<COMB_MAX && isnan(TOT_comb)==0  && TOT_comb>=1  &&  counter_inconsistent==0 % in this case the approch produces an acceptable set
    
    %% constrained max like solution thetagiven{gamma*}
    tic
    index_star = enumerate_integers( mu_gamS , P_gamS , GAMMA_gamS );
    %index_star = enumerate_integers(mu_gamma_init , P_gamma_init , GAMMA );
    if index_star == 0
      gamma_star = GAMMA; % there is a single element in GAMMA
    else
      gamma_star = GAMMA(:,index_star);
    end
    th_star = theta_given_gamma(gamma_star, A, At, delta, P_delta_inv, chords, Cl);  % constrained least squares estimate
    time_thgivengamma_star = toc;
    
    %% admissible solution set theta_set
    tic
    theta_set = theta_given_gamma(GAMMA, A, At, delta, P_delta_inv, chords, Cl);
    time_thgivengammas = toc;
    
    %% Solution that would have been obtained by rounding
    gamma_round = round(mu_gamma_init);
    [th_round, delta_round] = theta_given_gamma(gamma_round, A, At, delta, P_delta_inv, chords, Cl);
    disp('computed delta_round')
    
    %[lkth, ind_best] = evaluate_likelihood(At, delta, P_delta_inv, theta_set);
    
  else
    if TOT_comb>COMB_MAX || isnan(TOT_comb)==1  || TOT_comb<1
      TOT_comb
      error('INTEGER SCREENING FAILED TO PRODUCE A SMALL NUMBER OF CANDIDATES')
    else
      TOT_comb
      error('INCONSISTENCY: INTEGER SCREENING DID NOT PRODUCE ANY CANDIDATE')
    end
  end
  %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  if isempty (find(abs(round(mu_gamma_init) - gamma_star) > 10^-10 )) == 0 % if there is some element different from 0
    disp('ROUNDED estimate would have chosen differently +++++++++++++++++++')
  end
  
  if showTime == 1 % :-)
    disp('----------TIMING-----------')
    time_gammahat
    time_int_screening
    time_thgivengammas
    total_time = time_gammahat + time_int_screening + time_thgivengammas
  end
end
