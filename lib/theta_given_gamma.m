function [theta_set, delta_corr] = theta_given_gamma(GAMMA, A, At, delta, P_delta_inv, chords, Cl)
% Computes the orientation estimate and the corrected measurements given
% the integers GAMMA, as in the paper "L. Carlone and A. Censi, From Angular Manifolds to the 
% Integer Lattice: Guaranteed Orientation Estimation with Application to 
% Pose Graph Optimization, IEEE Trans. on Robotics, (30)2: 475-492, 2014."
% 
% Author: Luca Carlone
% Date: Jan 2013


delta = delta(:);
m = length(delta);
theta_set = [];

for i=1:size(GAMMA,2) % for each column of GAMMA, i.e., for each admissible gamma
    gamma_i = GAMMA(:,i);
    %warning('Checking determinant of Cl')
    %det(Cl)
    if nargin < 7
      k_i_lc = gamma_i;  
    else
      k_i_lc = Cl \ gamma_i;  
    end
    k_i = zeros(m,1);
    k_i(chords) = k_i_lc;
    nu = 2*pi * k_i;  % from integers to multiple of 2pi
    delta_corr = delta - nu; %application of the correction factors
    P_th_inv = A * P_delta_inv * At; % information matrix on th_sub
    th_so2 = P_th_inv  \ (A * P_delta_inv * delta_corr);
    theta_set(:,i) = th_so2;
end