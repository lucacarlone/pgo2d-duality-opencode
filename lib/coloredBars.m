function Fig = coloredBars(x, y, width, xlabels,fontL)

isPercentage = 1;
if isPercentage == 1
  scale = 100;
else
  scale = 1;
end

Fig = figure(); axes('FontSize', fontL); 
N = length(x);
for i=1:N
  yi = y(i);
  h = bar(x(i),yi,width); hold on; 
  if yi >= 1 * scale
    col = 'g';
  elseif yi < 1 * scale && yi >= 0.8 * scale
    col = 'y';
  elseif yi < 0.8 * scale && yi >= 0.5 * scale
    col = 'r';
  elseif yi < 0.5 * scale
    col = 'k';
  end
  set(h, 'FaceColor', col) 
end
set(gca, 'XTick', x);
set(gca, 'XTickLabel', xlabels);
set(Fig, 'paperunits', 'points' )
set(Fig,'position',[100 100, 600 450]); %
set(Fig,'papersize',[600 450]);
set(Fig,'PaperPositionMode','Auto')
% if min(y) <= 0.5
%   ylim([0.25 1.02]);
% else
%   ylim([0.5 1.02]);
% end
yw = 0.05;
ylim([0  1+yw] * scale);
xw = 0.5;
xlim([-xw length(x)-xw]);
grid on
box off

% set(gca, 'XTickLabel', '')  
% xlabetxt = uniNames(data(:,2));
% ylim([0 .5]); ypos = -max(ylim)/50;
% text(1:N,repmat(ypos,N,1), ...
%      xlabetxt','horizontalalignment','right','Rotation',90,'FontSize',15)
% text(.55,77.5,'A','FontSize',15)
% ylabel('median log2 fold change','FontSize',15)