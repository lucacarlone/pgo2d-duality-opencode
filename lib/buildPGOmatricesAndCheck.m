function [Wreal,Wcpx,Wcpx_r] = buildPGOmatricesAndCheck(edges, nrNodes, pstar, posesGNfromGT)

tol = 1e-6;

%% Build real PGO matrix Wreal (no anchor)
[~, ~, ~, N] = buildLinearModelPose2D(edges, nrNodes);
Wreal = N' * N; % real PGO matrix, no anchor
xrealStar = pose2DMatToVectPosCosSin(posesGNfromGT);

% Test 1: poseCost2D = xpr' Wreal xpr
% NB: here the vector xpr includes first positions and then rotation 
% (imposed by the function buildLinearModelPose2D, which builds N)
if isequalWithTol(pstar, xrealStar' * Wreal * xrealStar, 1e-4, 1e-8)==0
  pstar, xrealStar' * Wreal * xrealStar, error('mismatch in cost evaluation (1)')
end

%% Other way to build the same matrices (as a check)
[W, barA, barD, barQ] = buildW(edges); 
%% Test 2: these matrices should be identical
if norm(W - Wreal,'inf') > tol
  norm(W - Wreal,'inf'), error('mismatch between W and Wreal')
end
%% Test 2b: poseCost2D = x' Wcpx x (Calafiore14notes)
% NB: here the vector x includes 4 positions first rotation (cos sin) and then positions
% W:  real PGO matrix, no anchor (same as Wreal, but built blockwise)
if pstar > 1e-8 && norm(xrealStar' * W * xrealStar - pstar) / pstar > 0.001
  pstar, xrealStar' * W * xrealStar, norm(xrealStar' * W * xrealStar - pstar) / pstar, error('mismatch in cost evaluation (2)')
end

%% Build complex PGO matrix Wcpx (no anchor)
[Wcpx] = buildWtilde(edges);
%% Test 3: rotCost2D = xtilde' Wtilde xtilde (complex version)
im = sqrt(-1);
xcpx = realvec2complex(xrealStar);
if pstar > 1e-8 && norm(xcpx' * Wcpx * xcpx - pstar) / pstar > 0.01
  pstar, xcpx' * Wcpx * xcpx, error('mismatch in cost evaluation (2)')
end

%% Try to remove position anchor and verify that cost remains the same
Wcpx_r = Wcpx(2:end,2:end);
xcpx_r = [xcpx(1:nrNodes) - repmat(xcpx(1),nrNodes,1) ; xcpx(nrNodes+1:end)]; % we center the solution around p0
xcpx_r = xcpx_r(2:end); % remove node
if (pstar < tol && xcpx_r' * Wcpx_r * xcpx_r > tol) || (pstar > tol && norm(xcpx_r' * Wcpx_r * xcpx_r - pstar) / pstar > 0.01)
  pstar, xcpx_r' * Wcpx_r * xcpx_r, error('mismatch in cost evaluation (3)')
end