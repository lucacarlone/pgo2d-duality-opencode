function R = rot2D(th)

% Luca Carlone
% R = rot2D(th)  - th in radiants
% returns the 2 by 2 (planar) rotation matrix corresponding to the angle th

R = [cos(th)    -sin(th)
     sin(th)     cos(th)];