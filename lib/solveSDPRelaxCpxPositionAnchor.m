function [ddstar, XX] = solveSDPRelaxCpxPositionAnchor(Wcpx_r,nrNodes)

cvx_quiet(true);  
cvx_begin SDP
cvx_precision best
variable XX(2*nrNodes-1,2*nrNodes-1) complex semidefinite
minimize( trace(Wcpx_r * XX) )
subject to
for i=nrNodes:2*nrNodes-1
  XX(i,i) == 1;  
end
cvx_end
ddstar = trace(Wcpx_r * XX);