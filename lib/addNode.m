function [edges, thGT, posesGT, posesInit, nrNodes] = addNode(edges, thGT, posesGT, posesInit)
% Removes a single node from the graph: it assumes that the graph is a single loop
% Assumes that edges are ordered as (1,2), (2,3), ... , (n-1,n), (n,1)
% Node will be added at the end (n,i),(i,1)

N = length(thGT);
% edge to split
poseMeas_N_1 = edges(N,3:5)'; % this is the edge we have to split

% add node N+1 and edges (N,N+1),(N+1,1)
% WAY 1: along the edge
% poseMeas_N_Np1 = [poseMeas_N_1(1:2) * 0.4;  poseMeas_N_1(3)/2]; % split angle in half, cartesian is arbitrary

% WAY 2: along the edge but with zero angle
% poseMeas_N_Np1 = [rot2D(0.1)*poseMeas_N_1(1:2)/2;  0];

% WAY 3: random
% poseMeas_N_Np1 = [10*(rand(1,2)-0.5)  2*pi*(rand-0.5)]';

% WAY 4: fixed
%poseMeas_N_Np1 = [-4.8730e+00  -2.3108e+00   1.0000e+00]';
poseMeas_N_Np1 = [-3.8730e+00  -2.3108e+00   1.0000e+00]';

poseMeas_Np1_1 = pose_sub(poseMeas_N_1, poseMeas_N_Np1);
if norm( poseMeas_N_1 - pose_add(poseMeas_N_Np1,poseMeas_Np1_1) ) > 1e-7
  error('split error');
end
edges(end,:)   = [N   N+1 poseMeas_N_Np1' 1 0 0 1 0 1];
edges(end+1,:) = [N+1 1   poseMeas_Np1_1' 1 0 0 1 0 1];

%% fix other variables (fake extra node at zero)
poseNp1 = pose_add(posesGT(N,:)',poseMeas_N_Np1);
thGT = [thGT; poseNp1(3)];
posesGT = [posesGT; poseNp1'];
posesInit = [posesInit; poseNp1'];
nrNodes = N+1;

%% Old code
% Delta51 = delta56 + R56 delta61
% th56 = 0;
% th61 = poseMeas51(3);
% poseMeas56 = [poseMeas51(1:2)/2;  th56];
% poseMeas61 = [poseMeas51(1:2)/2;  th61];