function [Fig] = plotGraphPose2D(edges, poses, color, figHandler)

if nargin < 4
  Fig = figure(); axes('FontSize', 14); hold on
else
  figure(figHandler)
  Fig = figHandler;
end

cartesianBounds = 10;
% axis([-2 cartesianBounds+2 -2 cartesianBounds+2]);

m = size(edges,1);
for k=1:m
    i = edges(k,1); j = edges(k,2);
    plot(poses([i j],1),poses([i j],2),':k','LineWidth',1.5);
end

nrNodes  = size(poses,1);
for i=1:nrNodes
    plotPose2DTriangle(poses(i,:)', color);
    %text(poses(i,1),poses(i,2) + 1,num2str(i),'fontsize',18)
end

axis equal
marg = 0.5 * [-1 +1 -1 +1];
axis(axis + marg)
