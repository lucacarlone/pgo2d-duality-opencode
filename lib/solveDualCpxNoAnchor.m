function [dstar, lambda, WcpxLam, timeSolveSDPcpxNoAnchor] = solveSDPcpxNoAnchor(Wcpx,nrNodes)

tStart = tic;
cvx_quiet(true);  
cvx_begin SDP
cvx_precision best
variable lambda(nrNodes)
maximize( sum(lambda) )
subject to
Wcpx - diag([zeros(nrNodes,1) ; lambda]) >= 0;
cvx_end
timeSolveSDPcpxNoAnchor = toc(tStart);
fprintf('timeSolveSDPcpxNoAnchor: %g',timeSolveSDPcpxNoAnchor)

dstar = sum(lambda);
WcpxLam = Wcpx - diag([zeros(nrNodes,1) ; lambda]);
