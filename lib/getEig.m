function [V_W, D_W, eigW] = getEig(W)

W = full(W);
[V_W,D_W] = eig(W);
eigW = diag(D_W)';