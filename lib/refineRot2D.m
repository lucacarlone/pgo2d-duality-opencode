function thEst = refineRot2D(edges, thEst)

isDebug = 0;

m = size(edges,1);
nrNodes = length(thEst);
J = sparse(spalloc(2*m, nrNodes, 4*m));
fhat = sparse(zeros(2*m,1));
Mij = [-1 0; 0 -1];
cost = 1e+20;

if (isDebug) disp('Starting Gauss-Newton refinement'); end  
%% Gauss Newton iterations
for iter=1:20
  %% linerizeRot();
  costPrec = cost;
  for k=1:m
    id1 = edges(k,1);
    id2 = edges(k,2);
    Ri = rot2D(thEst(id1));
    Rj = rot2D(thEst(id2));
    Rij = rot2D(edges(k,5));
    rowInd = blockToMatIndices(k,2);
    inf_th = sqrt ( edges(k,11) ); % inverse of std for rotation measurements 
    fhat(rowInd) = inf_th *  ( Rij * Ri(:,1) + Mij * Rj(:,1) );
    J(rowInd,id1) = inf_th *  ( Rij * Ri(:,2) );
    J(rowInd,id2) = inf_th *  ( Mij * Rj(:,2) );
  end

  %% solveRot();
  Jobs = sparse(J(:,2:end));
  corrThEst = - (Jobs'*Jobs) \ (Jobs' * fhat);% minus because of fhat
  cost = norm(fhat)^2;
  relCostChange = abs((cost - costPrec)/costPrec);
  if (isDebug) fprintf('Current cost: %f, norm of the correction: %f, relative decrease: %f \n',cost,norm(corrThEst),relCostChange); end  
  thEst(2:end) = thEst(2:end) + corrThEst;
  
  %% check stopping conditions
  if norm(corrThEst) < 1e-3 || relCostChange < 1e-5
    break;
  end
end

end