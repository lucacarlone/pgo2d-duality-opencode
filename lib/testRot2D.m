clear all
clc
close all

th1 = 2 * pi * (rand - 0.5);
th2 = 2 * pi * (rand - 0.5);

R1 = rot2D(th1);
R2 = rot2D(th2);

R12 = R1*R2;

if norm(R12 - rot2D(th1+th2),'inf') > 1e-6
    error('something wrong (1)')
end

R1p2_expected = R1+R2
R1p2_actual = 2*cos((th1-th2)/2) * rot2D((th1+th2)/2)

if norm(R1p2_expected - R1p2_actual,'inf') > 1e-6
    error('something wrong (2)')
end


