function costAndPosesRank1 = rank1approxSDPrelax(XX, edges, nrNodes, GNrefinement)
% XX is a low rank matrix, and we want a rank 1 approximation, that 
% can be computed via SVD

x = rank1approx(XX);

%% Can be on poses or on rotations
if length(x) == nrNodes % rotation only submatrix
  rcpxEst_un = x; % non-normalized rotations
  pcpxEst_un = zeros(nrNodes,1); % positions unavailable
elseif length(x) == 2*nrNodes-1
  xcpxEstDual = x;
  pcpxEst_un = [0; xcpxEstDual(1:nrNodes-1)]; % non-normalized positions
  rcpxEst_un = xcpxEstDual(nrNodes:end); % non-normalized rotations
else
  error('incorrect dimention of x')
end

% Normalize rotations to have unit norm
abs_rcpxEst_un = abs(rcpxEst_un); % norm of the rotations
indZero = find(abs_rcpxEst_un < 1e-5);
if length(indZero) > 0 % some zero element
  cprintf('_red','found %d zero elements in abs_rcpxEstDual_un \n',length(indZero))
  abs_rcpxEst_un(indZero) = 1; % avoid division by zero
  rcpxEst_un(indZero) = 1; % set element to theta = 0, or cos = 1;
end
rcpxEst = rcpxEst_un./abs_rcpxEst_un; % normalize componentwise

% Estimate poses given rotations using GN
poseRank1Init = [real(pcpxEst_un) imag(pcpxEst_un)  phase(rcpxEst)];
if GNrefinement == 1
  posesEstVect = refinePose2D(edges, poseRank1Init(:,3), poseRank1Init);
  posesRank1 = pose2DvectToMat(posesEstVect);
else
  posesRank1 = poseRank1Init;
end

% Evaluate cost
costAndPosesRank1 = anchorAndCheckCost(posesRank1, edges, nrNodes);

