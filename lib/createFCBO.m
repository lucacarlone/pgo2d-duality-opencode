function [C, Cl] = createFCBO(edges_id)
% For a directed graph with n+1 nodes and m edges, the function computes 
% an l x m cycle basis matrix, with l = m-n. 
% - "edges_id" is an m x 2 matrix, such that each row [i j] encodes an edge
% starting from i and ending at j (assume the indices start from 1)
% NOTE: the function assumes that the first n rows of edges_id are 
% ordered as [1 2] [2 3] .. [n n+1] 
% 
% Luca Carlone
% Georgia Institute of Technology, USA
% Sept 20, 2014
%

m = size(edges_id,1);
n = max( max(edges_id(:,1)), max(edges_id(:,2)) ) - 1;
l = m-n;

% C = spalloc(l, m, l*(n+1)); %cycle basis matrix
% 
% for o=1:l   % for each loop
%   C(o, n+o)=+1; %chord
%   if edges_id(n+o,1)<edges_id(n+o,2)
%     et = [edges_id(n+o,1):edges_id(n+o,2)-1]; % edges in the spanning tree
%     C(o, et)=-1;
%   else
%     et = [edges_id(n+o,2):edges_id(n+o,1)-1];
%     C(o, et)=+1;
%   end
% end

%% Smart version: we create the matrix by columns with it more suitable for the 
%% compressed column format (then we transpose): much faster than the row-wise version
Ctran = spalloc(m, l, l*(n+1)); %cycle basis matrix traspose
for o=1:l   % for each loop
  Ctran(n+o, o)=+1; %chord
  if edges_id(n+o,1)<edges_id(n+o,2)
    et = [edges_id(n+o,1):edges_id(n+o,2)-1]; % edges in the spanning tree
    Ctran(et, o)=-1;
  else
    et = [edges_id(n+o,2):edges_id(n+o,1)-1];
    Ctran(et, o)=+1;
  end
end
C = Ctran';
Cl = speye(l);