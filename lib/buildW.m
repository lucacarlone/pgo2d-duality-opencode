function [W, barA, barD, barQ] = buildW(edges)

[nrNodes,m] = sizeDataset(edges);

%% create matrix W as:
% W1 = [barA  barD;  0  barU]
% W = W1' & W1

barD = spalloc(2*m,2*nrNodes,4*m); % 2 nz elements per row
barQ = zeros(2*nrNodes,2*nrNodes);
barU = zeros(2*m,2*nrNodes); % only for testing
[~,~,A] = reducedIncidenceMatrix(edges);
barA = kron(A,eye(2)); % create barA (augmented incidence matrix)

for k=1:m
    %% check measurement covariances
    if (edges(k,7)+edges(k,8)+edges(k,10)) ~= 0 % off diagonal terms must be zero
      error('Offdiagonal terms in measurement covariances are not supported in this phase')
    end
    if edges(k,6) ~= edges(k,9)
      error('Nonisotropic Cartesian covariance is not supported in this phase')
    end
    %% information for whitening
    infAng  =  sqrt(edges(k,11));
    infCart =  sqrt(edges(k,6));
    
    %% create barD
    Deltaij = edges(k,3:4)';
    DeltaijMat = [Deltaij(1)  -Deltaij(2); Deltaij(2)  Deltaij(1)];
    Rij = rot2D(edges(k,5));
    
    idk = blockToMatIndices(k,2);
    i = edges(k,1);
    idi = blockToMatIndices(i,2);
    barD(idk,idi) = - infCart * DeltaijMat; % multiplies rotations
    
    %% create barQ = barD'*barD + barU' * barU
    j = edges(k,2);
    idj = blockToMatIndices(j,2);
    deltaij = norm(Deltaij);
    barQ(idi,idi) = barQ(idi,idi) + (infAng^2 + infCart^2 * deltaij^2) * eye(2); 
    barQ(idj,idj) = barQ(idj,idj) + infAng^2 * eye(2); 
    barQ(idi,idj) = -infAng^2 *  Rij';
    barQ(idj,idi) = -infAng^2 *  Rij;
    
    barU(idk,idi) = -infAng * Rij; 
    barU(idk,idj) = infAng * eye(2);
    
    barA(idk,:) = infCart * barA(idk,:); % whitening 
end

%% check off diagonal terms of barQ, to check that only barU contributes there
testBarUtBarU = barU' *  barU - diag(diag(barU' *  barU)); 
testBarQ = barQ - diag(diag(barQ)); 
if norm(testBarUtBarU - testBarQ,'inf') > 1e-6
  barU' * barU, barQ, error('barUt *  barU - barQ test failed')
end
%% check of barQ = barD'*barD + barU' * barU
if norm(barQ - (barD'*barD + barU' * barU),'inf') > 1e-6
  norm(barQ - (barD'*barD + barU' * barU),'inf'), error('barQ - barD'*barD + barU' * barU')
end

%% check structure of barD'*barA
barB = zeros(2*nrNodes,2*nrNodes);
for i=1:nrNodes
    calNout_i = find(edges(:,1)==i);
    idi = blockToMatIndices(i,2);
    for k=[calNout_i]' % for each outgoing neighbor
        Deltaij = edges(k,3:4)';
        DeltaijMat = [Deltaij(1)  -Deltaij(2); Deltaij(2)  Deltaij(1)];
        barB(idi,idi) = barB(idi,idi) + infCart^2 * DeltaijMat';
        
        j = edges(k,2);
        idj = blockToMatIndices(j,2);
        barB(idi,idj) = - infCart^2 * DeltaijMat';
        %barB(idj,idi) = -DeltaijMat;
    end
end

if norm(barB - barD'*barA, 'inf') > 1e-5
    error('mismatch in barB - barD^T*barA')
end

%% assemble W
W = [barA'*barA   barA'*barD;  barD'*barA  barQ];

%% test
DtD = barD'* barD;
if norm(DtD - diag(diag(DtD)),'inf') > 1e-6
  error('DtD is not diagonal')
end
