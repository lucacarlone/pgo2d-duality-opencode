function poseVect3N = pose2DMatToVectPosCosSin(posesMat3xN)
% Transforms 3xN matrix with i-th row equal to [xi yi thetai] 
% into a vector describing a set of poses in the form
% poseVect3N = [x1 y1 x2 y2 ... xN yN theta1 theta2 ... thetaN]

if size(posesMat3xN,2) ~= 3
  error('wrong format for posesMat3xN')
end
nrNodes = size(posesMat3xN,1); % number of rows of the matrix
xp = zeros(2*nrNodes,1); % preallocation of positions
for i=1:nrNodes
  xp(2*i-1:2*i) = posesMat3xN(i,1:2)';
end
xr = zeros(2*nrNodes,1); % preallocation of rotations as CosSin
for i=1:nrNodes
  xr(2*i-1:2*i) = [cos(posesMat3xN(i,3)) sin(posesMat3xN(i,3))]';
end
poseVect3N = [xp(:); xr(:)];