function [edges, thGT, posesGT, posesInit] = buildGridDataset2D(nrRows, nrCols, probLC, sigmaT, sigmaR, scale) 

% addpath(genpath('../'));
doPlot = 0;

if(doPlot)
  figure(100)
  hold on;
end

% create GT poses
posesGT = zeros(nrRows*nrCols,3);
corners = [];
nrNodes = 0;
for i=1:nrRows
  for j=1:nrCols
    nrNodes = nrNodes+1; % we add a new node
    if i==1 && j ==1 
      th = 0;
    else
      th = 2*pi*rand - pi; % random orientation
    end
    if rem(i,2)==1 % odd rows
      posesGT(nrNodes,:) = [j, i, th];
    else
      posesGT(nrNodes,:) = [nrCols-j+1, i, th];
    end
    %plot( poses(nrNodes,1), poses(nrNodes,2),'o'); hold on
    if i==nrRows || j==1 || j==nrCols
      corners = [corners nrNodes];
      %plot( poses(nrNodes,1), poses(nrNodes,2),'ro'); hold on
    end
  end
end

% scaling
if scale~=1
  fprintf(' --- scaling cartesian components by %g\n',scale)
end
posesGT(:,1:2) = scale * posesGT(:,1:2); % scale only the cartesian part

% create odometric edges
m = nrNodes-1;
for i=1:m
  id1 = i;
  id2 = i+1;
  edges(i,1:2) = [id1 id2];
  p1 = posesGT(id1, 1:3); % gt positions
  p2 = posesGT(id2, 1:3);
  edges(i, 3:5) = poseSubNoisy(p2 , p1, sigmaT, sigmaR);
  edges(i, 6:11) = [1/sigmaT^2 0 0 1/sigmaT^2 0 1/sigmaR^2];
  if(doPlot) plot( [p1(1) p2(1)], [p1(2) p2(2)],'-k','LineWidth',2); end
end

% create loop closures
for id1=[1:nrNodes]
  p1 = posesGT(id1, 1:3); % gt positions
  for id2=[id1+1:nrNodes] %% we look for all possible neighbors (unit distance)
    p2 = posesGT(id2, 1:3);
    if ( norm(p1(1:2) - p2(1:2)) < 1+0.01 && norm(p1(1:2) - p2(1:2)) > 0.01 ) % unit distance but not the same node
      edge = [id1 id2];
      if (id1==id2) error('coincident nodes?'); end
      if rand<probLC && findUndirectedEdge(edge, edges) == 0 % add edge with some probability if it is not there yet
        m = m+1;
        edges(m,1:2) = [id1 id2];
        edges(m, 3:5) = poseSubNoisy(p2 , p1, sigmaT, sigmaR);
        edges(m, 6:11) = [1/sigmaT^2 0 0 1/sigmaT^2 0 1/sigmaR^2];
        if(doPlot) plot( [p1(1) p2(1)], [p1(2) p2(2)],'--.','LineWidth',1.5); end
      end
    end
  end
end

thGT = posesGT(:,3);
posesInit = odometryFromEdges(edges,nrNodes);
