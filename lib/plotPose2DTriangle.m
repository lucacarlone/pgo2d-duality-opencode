function [] = plotPose2DTriangle(pose, lineColor)

base = 0.7;
triangle = [-base/4  -base/4   base;
            -base/2  +base/2   0];

R_i = rot2D(pose(3));
localTriangle = R_i * triangle + kron( pose(1:2), ones(1,3));

fill( [ localTriangle(1,:) localTriangle(1,1)], [ localTriangle(2,:) localTriangle(2,1)],'b')
% plot([ localTriangle(1,:) localTriangle(1,1)], ...
%      [ localTriangle(2,:) localTriangle(2,1)], '-', 'color', lineColor,'lineWidth',2);

uv = R_i * [2*base;0];
quiver(pose(1),pose(2),uv(1),uv(2),'r','lineWidth',2);
uv = R_i * [0;base];
quiver(pose(1),pose(2),uv(1),uv(2),'g','lineWidth',2);