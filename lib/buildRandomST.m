function [edges, thGT, posesGT, posesEst] = buildRandomST(nrNodes, isLineGraph, isUniform, sigmaR, sigmaT, scale)
% Creates random tree, with a pose associated to nodes and relative pose
% measurements associated to eges
% 
% INPUTS:
% - nrNodes: number of nodes in the tree
% - isLineGraph: if set to 1 creates a spanning path, otherwise a spanning tree
% - isUniform: use uniform, rather than gaussian noise
% - sigmaR: std of rotation noise
% - sigmaT: std of translation noise
% - scale: multiplies the translation part by a scale factor
% OUTPUTS:
% - edges: m x 11 matrix describing edges (in g2o format): each row, describe edge (i,j) 
%          and is in the form [i j pij Ixx Ixy Ixth Iyy Ixth Ithth] where i
%          and j are the node id, pij = [x y th] is the 2D measured relative pose and
%          I describe the entries of the information matrix describing
%          measurement noise
% - thGT: ground truth orientation of the nodes generating the measurements
% - posesGT: ground truth poses of the nodes generating the measurements
% - posesEst: pose estimate (odometry) obtained  by concatenating measurements
%
% Author: Luca Carlone
% Date: 2015-1-1
% Institution: Georgia Institute of Technology
%

if nargin<3
  isLineGraph = 0;
  isUniform = 0;
end
if nargin < 5
  sigmaT = 0.01;
  sigmaR = 0.01;
end
if isUniform
  cprintf('_cyan','Uniform noise on pose measurements\n')
end
cartesianBounds = scale * 10;

%% Random ground truth poses in [cartesianBounds]
posesGT = zeros(nrNodes,3);
for i=1:nrNodes
    xy = cartesianBounds * rand(1,2);
    th = 2*pi * (rand-0.5); % in [-pi,pi]
    posesGT(i,:) = [xy th];
end

%% edges, to form a spanning path (isLineGraph==1) or a generic tree (isLineGraph==0)
edges = [];
if isLineGraph==1
  for k=1:nrNodes-1 % edges in the spanning tree
    p_i = posesGT(k,:)';
    p_j = posesGT(k+1,:)';    
    deltaPoseNoisy = poseSubNoisy(p_j , p_i, sigmaT, sigmaR, isUniform);
    edges(end+1,1:11) = [k k+1 deltaPoseNoisy' 1/sigmaT^2 0 0 1/sigmaT^2 0 1/sigmaR^2];
  end
else
  % do something smarter to create a tree structure
  connectedElements = zeros(nrNodes,1);
  connectedElements(1) = 1; % root
  prob = 0.3;
  
  while min(connectedElements)==0 % until we connect everything
    initialDisconnectedNodes = length(find(connectedElements==0));
    nodesToExpand = find(connectedElements==1)';
    for i=[nodesToExpand] % try to expand the nodes that are already connected
      while rand < prob || length(find(connectedElements==1))==1 % last node to expand
        disconnectedNodes = find(connectedElements==0);
        if (length(disconnectedNodes)==0) break; end % already done
        % pick one
        jind = randi(length(disconnectedNodes));
        j = disconnectedNodes(jind);
        p_i = posesGT(i,:)';
        p_j = posesGT(j,:)';
        deltaPoseNoisy = poseSubNoisy(p_j , p_i, sigmaT, sigmaR, isUniform);
        edges(end+1,1:11) = [i j deltaPoseNoisy' 1/sigmaT^2 0 0 1/sigmaT^2 0 1/sigmaR^2];
        connectedElements(j) = 1; % this node is now connected
      end
      connectedElements(i) = 2; % we have already expanded this node
    end
    if length(find(connectedElements==0)) == initialDisconnectedNodes
      edges, error('Something wrong, we are not making progress')
    end
  end
end

thGT = posesGT(:,3);
posesEst = odometryFromEdges(edges,nrNodes); % composing measurements along ST