function [nrNodes,m,l,n] = sizeDataset(edges)
% Author: Luca Carlone
% Date: 2014-11-5
%
% input: edges is a matrix whose first two columns are in the form [i j] 
% where (i,j) is a directed edge in a graph
% output: [nrNodes,m,l,n]

nrNodes = max ( max(edges(:,1)), max(edges(:,2)) );
n = nrNodes - 1; % number of nodes - 1
m = size(edges,1);
l = m-n;