function [A, b, AwoRotAnc, AwoAnc] = buildLinearModelPose2D(edges, nrNodes)
% builds a quadratic problem Ax - b, with x = [p, r], and p in R^2(n-1) 
% and r in R^2(n-1) (we fixed a rotation to [cos=1 sin=0] and a position to [0 0])
% Therefore A in R^(4m X 4(n-1))

m = size(edges,1); % num measurements
Ar = spalloc(4*m, 2*nrNodes, 4*4*m); 
Ac = spalloc(4*m, 2*nrNodes, 4*4*m); 

for k=1:m %[i j dx dy dth]
  Deltaij = edges(k,3:4)';
  DeltaijMat = [Deltaij(1)  -Deltaij(2); Deltaij(2)  Deltaij(1)];
  
  Rij = rot2D(edges(k,5));
  indRow = blockToMatIndices(k, 4);
  
  %% here we do not need the 1/2, since is [cos sin] instead of rotation
  inf_th = sqrt ( edges(k,11) ); % inverse of std for rotation measurements 
  inf_cart = sqrt ( edges(k,6) ); % inverse of std for position measurements 
  
  i = edges(k,1);
  indCol = blockToMatIndices(i, 2);
  Ac(indRow, indCol) = [-inf_cart*eye(2)     ; zeros(2,2)];
  Ar(indRow, indCol) = [-inf_cart*DeltaijMat ; inf_th*Rij]; % original cost has a factor 2 in front of the rotation part
  
  j = edges(k,2);
  indCol = blockToMatIndices(j, 2);
  Ac(indRow, indCol) = [ inf_cart*eye(2)     ; zeros(2,2)];
  Ar(indRow, indCol) = [  zeros(2,2)         ; -inf_th*eye(2)]; % rotj does not appear in the cartesian part
end

AwoAnc = [Ac Ar]; % all positions and all orientions

Ac = Ac(:,3:end); % we exclude the position anchor
AwoRotAnc = [Ac Ar]; % all positions-1 and all orientions (position anchor)

b = -Ar(:,1); % first column, as orientation of node 0 is [cos; sin] = [1; 0];
Ar = Ar(:,3:end); % we exclude the rotation anchor
A = [Ac Ar];