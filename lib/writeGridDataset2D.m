function [nrNodes] = writeGridDataset2D(nrRows, nrCols, probLC, sigmaT, sigmaR, filename) 

addpath(genpath('../'));
doPlot = 1;

if nargin < 1
  nrRows = 5;
  nrCols = 5;
  probLC = 0.3;
  sigmaT = 0.01;
  sigmaR = 0.02;
  filename = horzcat('/home/aspn/Desktop/TRO/MOLE2Dmatlab/cleanDatasets/2D/grid2D.g2o');
end

if(doPlot)
  figure
  hold on;
end

% create GT poses
poses = zeros(nrRows*nrCols,3);
corners = [];
nrNodes = 0;
for i=1:nrRows
  for j=1:nrCols
    nrNodes = nrNodes+1; % we add a new node
    if i==1 && j ==1 
      th = 0;
    else
      th = 2*pi*rand - pi; % random orientation
    end
    if rem(i,2)==1 % odd rows
      poses(nrNodes,:) = [j, i, th];
    else
      poses(nrNodes,:) = [nrCols-j+1, i, th];
    end
    %plot( poses(nrNodes,1), poses(nrNodes,2),'o'); hold on
    if i==nrRows || j==1 || j==nrCols
      corners = [corners nrNodes];
      %plot( poses(nrNodes,1), poses(nrNodes,2),'ro'); hold on
    end
  end
end

% create odometric edges
m = nrNodes-1;
for i=1:m
  id1 = i;
  id2 = i+1;
  edges_id(i,1:2) = [id1 id2];
  p1 = poses(id1, 1:3); % gt positions
  p2 = poses(id2, 1:3);
  edges_id(i, 3:5) = poseSubNoisy(p2 , p1, sigmaT, sigmaR);
  edges_id(i, 6:11) = [1/sigmaT^2 0 0 1/sigmaT^2 0 1/sigmaR^2];
  if(doPlot) plot( [p1(1) p2(1)], [p1(2) p2(2)],'-k','LineWidth',2); end
end

% create loop closures
for id1=[1:nrNodes]
  p1 = poses(id1, 1:3); % gt positions
  for id2=[id1+1:nrNodes] %% we look for all possible neighbors (unit distance)
    p2 = poses(id2, 1:3);
    if ( norm(p1(1:2) - p2(1:2)) < 1+0.01 && norm(p1(1:2) - p2(1:2)) > 0.01 ) % unit distance but not the same node
      edge = [id1 id2];
      if (id1==id2) error('coincident nodes?'); end
      if rand<probLC && findUndirectedEdge(edge, edges_id) == 0 % add edge with some probability if it is not there yet
        m = m+1;
        edges_id(m,1:2) = [id1 id2];
        edges_id(m, 3:5) = poseSubNoisy(p2 , p1, sigmaT, sigmaR);
        edges_id(m, 6:11) = [1/sigmaT^2 0 0 1/sigmaT^2 0 1/sigmaR^2];
        if(doPlot) plot( [p1(1) p2(1)], [p1(2) p2(2)],'--.','LineWidth',1.5); end
      end
    end
  end
end

odometryFromEdges(edges_id,nrNodes,2);
writeG2oDataset2D(filename, poses, edges_id);