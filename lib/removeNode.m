function [edges, thGT, posesGT, posesInit, nrNodes] = removeNode(edges, thGT, posesGT, posesInit,nodeToRemove)
% Removes a single node from the graph: it assumes that the graph is a single loop
% Assumes that edges are ordered as (1,2), (2,3), ... , (n-1,n), (n,1)

nrNodes = length(thGT);
m = size(edges,1);
k = nodeToRemove;
% we have to remove edges (k-1,k) and (k,k+1), which are in row k-1 and k of edges
% we use the notation k-1=i and k+1=j hence the edges are (i,k) and (k,j)
i = k-1; if(i==0)         i=nrNodes; end 
j = k+1; if(j==nrNodes+1) j=1;       end

%% Fix edges
poseMeas_ik = edges(i,3:5)';
poseMeas_kj = edges(k,3:5)';
poseMeas_ij = pose_add(poseMeas_ik,poseMeas_kj);
edges = edges(setdiff([1:m],[i k]),:);
edges(end+1,:) = [i j poseMeas_ij' 1 0 0 1 0 1];
% fix indices
m = m-1; % we removed an edge
for o=1:m
  if edges(o,1)>k
    edges(o,1) = edges(o,1)-1;
  end
  if edges(o,2)>k
    edges(o,2) = edges(o,2)-1;
  end
end
% sort edges
[~,order] = sort(edges(:,1));
edges = edges([order],:);

%% Fix GT and initial guess
nodesToKeep = setdiff([1:nrNodes],k);
thGT = thGT(nodesToKeep);
posesGT = posesGT(nodesToKeep,:);
posesInit = posesInit(nodesToKeep,:);
nrNodes = nrNodes-1;
end