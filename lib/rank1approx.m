function x = rank1approx(XX)

%% Find best rank 1 approximation for XX
[U,D,V] = svd(XX);
% test positive semidefiniteness
if min(eig(XX)) < -1e-7
  error('negative eigenvalue in XX')
end
if D(1,1) ~= max(diag(D))
  error('Singular values are not ordered')
end
%% This is the best rank 1 approximation
XXrank1 = U(:,1) * D(1,1) * V(:,1)'; 
% test positive semidefiniteness
minEigXXrank1 = min(real(eig(XXrank1)));
if minEigXXrank1 < -1e-7
  error('negative eigenvalue in XXrank1')
end
% fix numerical issues
if minEigXXrank1 < 0
  cprintf('_red','(disabled) making smallers eig positive by diagonal perturbation (minEig=%g) \n',minEigXXrank1);
  % SURE? XXrank1 = XXrank1 + 1e-10 * eye(size(XX));
end
% test symmetry:
if norm(XXrank1 - XXrank1')>1e-5
  error('nonsymmetric XXrank1')
end
% test real diagonal entries:
for i=1:size(XXrank1,1)
  if abs(imag(real(XXrank1(i,i))))>1e-5
    error('non real elements on the diagonal of XXrank1')
  end
end
%% symmetrize :-)
XXrank1 = (XXrank1 + XXrank1') / 2;
xMat = cholcov(XXrank1);
if norm(xMat' * xMat - XXrank1)>1e-5
  error('cholcov failed')
end
normRows_xMat = zeros(size(xMat,1),1);
for i=1:size(normRows_xMat,1)
  normRows_xMat(i) = norm(xMat(i,:));
end
%% find numerical nonzero row
ind_x = find(normRows_xMat > 1e-2); 
if length(ind_x)>1
  normRows_xMat(ind_x)
  warning('xMat is not close to a matrix with a single nonzero row')
  pause(20)
  [~,ind_x] = max(normRows_xMat);
end
x = xMat(ind_x,:); 
if norm(x' * x - XXrank1) > 1e-4
  norm(x' * x - XXrank1), error('rank 1 failed')
end
x = x(:); % as a column vector