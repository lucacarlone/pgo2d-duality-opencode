function thInitialGuess = computeInitialGuess2D(edges)

nrNodes = sizeDataset(edges);

computedElements = zeros(nrNodes,1);
thInitialGuess = nan(nrNodes,1);
thInitialGuess(1) = 0;
computedElements(1) = 1;

while min(computedElements)==0
    initialMissingElements = length(find(computedElements==0));
    computedNodes = find(computedElements==1)';
    for i=[computedNodes] % try to expand the estimate along edges
        outgoingEdges = find(edges(:,1)==i)';
        for k=outgoingEdges
            id2 = edges(k,2);
            if computedElements(id2) == 0
                dth = edges(k,5);
                thInitialGuess(id2) = thInitialGuess(i) + dth;
                computedElements(id2) = 1;
            end
        end
        incomingEdges = find(edges(:,2)==i)';
        for k=incomingEdges
            id1 = edges(k,1);
            if computedElements(id1) == 0
                dth = edges(k,5);
                thInitialGuess(id1) = thInitialGuess(i) - dth;
                computedElements(id1) = 1;
            end
        end
        computedElements(i) = 2; % we expanded this node, so we do not have to
    end
    if length(find(computedElements==0)) == initialMissingElements
        edges
        error('Something wrong, we are not making progress')
    end
end