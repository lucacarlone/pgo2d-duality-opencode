disp('===========================================')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE POSE GRAPH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch scenario
  %% Random runs
  case 'Graph' % Create random graph
    [edges, thGT, posesGT, posesInit] = buildRandomGraph(nrNodes, isUniform, sigmaR, sigmaT, pLC, scale);
  case 'Grid' % Create random graph
    [edges, thGT, posesGT, posesInit] = buildGridDataset2D(sqrt(nrNodes), sqrt(nrNodes), pLC, sigmaT, sigmaR, scale); 
  case 'ST' % Create random ST
    isLineGraph = 0;  [edges, thGT, posesGT, posesEst]= buildRandomST(nrNodes, isLineGraph, isUniform, sigmaR, sigmaT);
  case 'Cycle' % Create random loop
    edges_id = zeros(nrNodes,2);
    edges_id(:,1) = [[1:nrNodes-1]'; nrNodes];
    edges_id(:,2) = [[2: nrNodes ]'; 1];
    [edges, thGT, posesGT, posesInit] = buildRandomMeasurements2D(edges_id, isUniform, sigmaR, sigmaT,'circle');
  case 'FixedTopology' % fixed topology & random measurements
    load dualfailsGC;
    [edgesQ, thSO2, nrNodes] = buildGraphFromQtilde(Q_4);
    fprintf('Fixed number of nodes: %d\n', nrNodes)
    edges_id = edgesQ(:,1:2);
    [edges, thGT, posesGT, posesInit] = buildRandomMeasurements2D(edges_id, isUniform, sigmaR, sigmaT,'circle');
    % edges = edgesQ; % thGT = thSO2; % posesGT = [zeros(nrNodes,2), thSO2];
    
  %% Saved examples
  case 'SingleLoopFail'% Load from file (single loop fail)
    load(singleLoopFile,'edges', 'thGT', 'posesGT', 'posesInit')
    nrNodes = length(thGT);
  case 'TwoLoopFail'% Load from file 
    load(twoLoopFile,'edges', 'thGT', 'posesGT', 'posesInit')
    nrNodes = length(thGT);
  case 'SingleLoopMerge'
    load(singleLoopFile,'edges', 'thGT', 'posesGT', 'posesInit')
    nodesToRemove = 5
    [edges, thGT, posesGT, posesInit, nrNodes] = removeNode(edges, thGT, posesGT, posesInit,nodesToRemove);
  case 'SingleLoopSplit' % Load from file (single loop split)
    load(singleLoopFile,'edges', 'thGT', 'posesGT', 'posesInit')
    [edges, thGT, posesGT, posesInit, nrNodes] = addNode(edges, thGT, posesGT, posesInit);
  case 'SingleLoopAddEdge' 
    load(singleLoopFile,'edges', 'thGT', 'posesGT', 'posesInit')
    i = 2;
    j = 4;
    p_i = posesGT(i,:)';
    p_j = posesGT(j,:)';
    deltaPoseNoisy = poseSubNoisy(p_j , p_i, sigmaT, sigmaR, isUniform);
    edges(end+1,1:11) = [i j deltaPoseNoisy' 1 0 0 1 0 1];
    nrNodes = length(thGT);
  case 'SingleLoopScaling' % Load from file (single loop scaling)
    load(singleLoopFile,'edges', 'thGT', 'posesGT', 'posesInit')
    scale = condSet(cond);
    edges(:,3:4) = scale * edges(:,3:4);
    nrNodes = length(thGT);
  otherwise
    error('invalid choice of scenario')
end

% save('test4-singleLoopFail-n5.mat','edges', 'thGT', 'posesGT', 'posesInit')
% save('test5-2LoopsFail-n4.mat','edges', 'thGT', 'posesGT', 'posesInit')
% save('test6-singleLoopFail-addEdge.mat','edges', 'thGT', 'posesGT', 'posesInit')

%% Plot before centering & Check type of graph
if doPlot
  Fig = figure(1); clf; axes('FontSize', 14); hold on
  plotGraphPose2D(edges, posesGT, 'b', Fig); %[0 0.5 0]);
  if doPrint
    set(Fig, 'paperunits', 'points' )
    set(Fig,'position',[100 100, 600 450]); %
    set(Fig,'papersize',[600 450]);
    set(Fig,'PaperPositionMode','Auto')
    filename = ('PGOtest.eps')
    saveas(Fig,filename);
  end
end

%% Center graph in (0,0,0)
anchor = posesGT(1,:)';
for i=1:nrNodes
  posesGT(i,:) = poseSubNoisy(posesGT(i,:)', anchor, 0 , 0)';
end

istree = 0; singleLoop = 0;
if size(edges,1) == nrNodes - 1
  istree = 1;
  disp('-- Graph is a tree')
end
if size(edges,1) == nrNodes
  singleLoop = 1;
  disp('-- Graph has a single loop')
end