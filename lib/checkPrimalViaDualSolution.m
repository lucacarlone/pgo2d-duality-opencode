function checkPrimalViaDualSolution(V_WcpxLam_r, Wcpx_r, nrNodes, dstar)

vPose2 = V_WcpxLam_r(:,1); % eigenvector corresponding to zero eigenvalue
vpos = vPose2(1:nrNodes-1);
vrot = vPose2(nrNodes:end);
vnorm = abs(vrot(1));
for i=1:nrNodes
  if abs( abs(vrot(i)) - vnorm ) / vnorm > 0.05 || vnorm < 1e-5
    abs(vrot), abs( abs(vrot(i)) - vnorm ) / vnorm, warning('Pose2D: SZEP but eigenvector cannot produce a feasible solution')
  else
    vPose2norm = [vpos; vrot]/vnorm;
    pstarViaDualPose2D = abs(vPose2norm' * Wcpx_r * vPose2norm);
    relError = norm(pstarViaDualPose2D - dstar) / dstar;
    if isequalWithTol(dstar, pstarViaDualPose2D, 1e-1, 1e-4) == 0
      dstar, pstarViaDualPose2D, relError, warning('Pose2D: suboptimal construction of primal optimal solution via the dual')
    end
  end
end