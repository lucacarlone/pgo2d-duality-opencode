function rotCost2D = evaluateFrobeniousCostRot2D(edges, thEst)

m = size(edges,1);

rotCost2D = 0;
for k=1:m 
  infThSq = edges(k,11);
  id1 = edges(k,1);  
  id2 = edges(k,2);
  Ri = rot2D(thEst(id1));
  Rj = rot2D(thEst(id2));
  Rij = rot2D(edges(k,5));
  X = Ri * Rij - Rj;
  rotCost2D = rotCost2D + 0.5* infThSq * norm(X,'fro')^2;
end