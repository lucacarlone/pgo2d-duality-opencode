function [evectors,evalues] = myeig(X, nrEigValues,str)

if strcmp(str,'SM')==0
  error('myeig only supports SM right now')
end

if nrEigValues~=1
  error('myeig only supports nrEigValues==1 right now')
end

[V,D] = eig(full(X));
Dvector = diag(D);
[evalues,evaluesInd] = min(Dvector);
evectors = V(:,evaluesInd);


