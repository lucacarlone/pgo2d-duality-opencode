function xtilde = realvec2complex(x)

im = sqrt(-1);
n = length(x)/2;
xtilde = zeros(n,1);
for i=1:n
  th = atan2(x(2*i) , x(2*i-1) ); 
  xtilde(i) = norm(x(2*i-1:2*i)) * exp( im * th );
  if isnan(xtilde(i))
    error('isnan(xtilde(i))')
  end
end