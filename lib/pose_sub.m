function [delta_pose] = pose_sub(p_j , p_i)
% Luca Carlone
% pose_sub = (p_j , p_i) = inv(p_i) * p_j 
% returns the planar roto-translation that tranform pj in pi

p_j = p_j(:);
p_i = p_i(:);

rho_i = p_i(1:2);
rho_j = p_j(1:2);
th_i = p_i(3);
th_j = p_j(3);

R = rot2D(th_i);

delta_th = wrapToPi(th_j - th_i);

if(abs(delta_th) > pi)
    disp('Error in pose_sub')
   disp(delta_th) 
end

delta_pose = [ R' * (rho_j   -  rho_i)   
                  delta_th  ];
