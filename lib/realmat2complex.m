function mtilde = realmat2complex(M)

if size(M,1)~=2 || size(M,2)~=2
   error('realmat2complex only works for 2x2 matrices'); 
end
if M(1,1)~=M(2,2)
   error('M(1,1)~=M(2,2)'); 
end
if M(1,2)~=-M(2,1)
   error('M(1,2)~=-M(2,1)'); 
end

%% if matrix is ok we transform it to a complex number
im = sqrt(-1);
a = norm([M(1,1) , M(2,1)]);
if a > eps
  M = M/a;
  th = atan2(M(2,1) , M(1,1) ); 
  mtilde = a * exp( im * th );
else
  mtilde = 0;
end

if isnan(mtilde)
  error('isnan(mtilde)')
end
