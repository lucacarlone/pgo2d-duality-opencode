function [Wtilde, barAtilde, barDtilde, barQtilde] = buildWtilde(edges)

[nrNodes,m] = sizeDataset(edges);

barDtilde = spalloc(m,nrNodes,m);
barQtilde = sparse(zeros(nrNodes,nrNodes));
barU = spalloc(m,nrNodes,2*m); 
% create barA (augmented incidence matrix)
[~,~,barAtilde] = reducedIncidenceMatrix(edges);

%% Populate matrices
for k=1:m
  %% check measurement covariances
  if (edges(k,7)+edges(k,8)+edges(k,10)) ~= 0 % off diagonal terms must be zero
    error('Offdiagonal terms in measurement covariances are not supported in this phase')
  end
  if edges(k,6) ~= edges(k,9)
    error('Nonisotropic Cartesian covariance is not supported in this phase')
  end
  %% information for whitening
  infAng  =  sqrt(edges(k,11));
  infCart =  sqrt(edges(k,6));
  
  if (isnan(infAng) || isnan(infCart))
    error('isnan(infAng) || isnan(infCart)')
  end
  
  %% whiten barAtilde
  barAtilde(k,:) = infCart * barAtilde(k,:); % whitening
  
  %% create barD
  Deltaij = edges(k,3:4)';
  DeltaijMat = [Deltaij(1)  -Deltaij(2); Deltaij(2)  Deltaij(1)];
  Rij = rot2D(edges(k,5));
  
  i = edges(k,1);
  barDtilde(k,i) = - infCart * realmat2complex(DeltaijMat);
  
  %% create barQ
  j = edges(k,2);
  deltaij = norm(Deltaij);
  barQtilde(i,i) = barQtilde(i,i) + (infAng^2 + infCart^2 * deltaij^2);
  barQtilde(j,j) = barQtilde(j,j) + infAng^2;
  barQtilde(i,j) = barQtilde(i,j) - infAng^2 * realmat2complex(Rij');
  barQtilde(j,i) = barQtilde(j,i) - infAng^2 * realmat2complex(Rij);
  
  %% create barU
  barU(k,i) = - infAng * realmat2complex(Rij);
  barU(k,j) = infAng;
end

% for debug
testBarQtilde = barU'*barU + barDtilde'*barDtilde;
if max(max(abs(testBarQtilde - barQtilde))) > 1e-3
  save logU
  max(max(abs(testBarQtilde - barQtilde))), error('norm(testBarQtilde - barQtilde)')
end

%% assemble W
Wtilde = [barAtilde'*barAtilde   barAtilde'*barDtilde;   barDtilde'*barAtilde  testBarQtilde];
