function [dstar_r, lambda_r, WcpxLam_r, timeSolveSDPcpxPosAnchor] = solveDualCpxPositionAnchor(Wcpx_r,nrNodes)

tStart = tic;
cvx_quiet(true);  
cvx_begin SDP
cvx_precision best
variable lambda_r(nrNodes)
maximize( sum(lambda_r) )
subject to
Wcpx_r - diag([zeros(nrNodes-1,1) ; lambda_r]) >= 0;
cvx_end
timeSolveSDPcpxPosAnchor = toc(tStart);
fprintf('timeSolveSDPcpxPosAnchor: %g\n',timeSolveSDPcpxPosAnchor)

dstar_r = sum(lambda_r);
WcpxLam_r = Wcpx_r - diag([zeros(nrNodes - 1,1) ; lambda_r]);