function [cost2D, rotCost, cartCost] = evaluateFrobeniousCostPose2D(edges, poseEst)

[nrNodes,m] = sizeDataset(edges);
if size(poseEst,1)==nrNodes && size(poseEst,2) == 3
    % poses given as a nrNodes x 3 matrix
    temp = zeros(3*nrNodes,1); % we want the input to be a vector, with positions
    % first (2*nrNodes entries) and rotations later (nrNodes entries)
    for i=1:nrNodes
        idi = blockToMatIndices(i,2);
        temp(idi) = poseEst(i,1:2)'; % positions
        temp(2*nrNodes+i) = poseEst(i,3); % rotations
    end
    poseEst = temp;
end

thEst = poseEst(2*nrNodes+1:end); % first 2NrNodes elements are xy, 
if length(thEst)~=nrNodes % last NrNodes elements are the angle estimates
  error('evaluateFrobeniousCostPose2Dwrong dimension of thEst')
end

cost2D = 0;
rotCost = 0;
cartCost = 0;
for k=1:m % for each measurement
  id1 = edges(k,1);
  id2 = edges(k,2);
  Ri = rot2D(thEst(id1));
  Rj = rot2D(thEst(id2));
  Rij = rot2D(edges(k,5));
  Deltaij = edges(k,3:4)';
  
  inf_th = sqrt ( edges(k,11) ); % inverse of std for rotation measurements 
  inf_cart = sqrt ( edges(k,6) ); % inverse of std for position measurements 
  
  X = inf_th * (Ri * Rij - Rj);
  id1c = blockToMatIndices(id1,2);
  id2c = blockToMatIndices(id2,2);
  Xcart =  inf_cart * (poseEst(id2c) - poseEst(id1c) - Ri * Deltaij);
  
  %% 1/2 in front of the frobenius norm, as here we are working on matrices
  cost2D = cost2D + 0.5 * norm(X,'fro')^2 + norm(Xcart)^2;
  
  rotCost = rotCost   + norm(X,'fro')^2;
  cartCost = cartCost + norm(Xcart)^2;
end

if abs ( cost2D - (0.5*rotCost+cartCost) ) >1e-4
  error('Cost mismatch')
end