function [pstarViaDual, posesEstDual, otherRelaxSol] = solvePrimalViaDual(Wcpx_r, WcpxLam_r, XX, edges, nrNodes, zeroMultiplicityPose2D, otherRelaxSol,GNrefinement)

doV2 = 1; % perform verification v2

fprintf('solvePrimalViaDual: GNrefinement = %d, doV2 = %d \n',GNrefinement,doV2)
  
if zeroMultiplicityPose2D == 1
  %% GUARANTEED OPTIMAL SOLUTION, when SZEP is true
  [xcpxEstDual,~] = myeig(WcpxLam_r,1,'SM');
  xcpxEstDual = xcpxEstDual./abs(xcpxEstDual(end));
  pcpxEstDual = [0; xcpxEstDual(1:nrNodes-1)]; % normalized
  rcpxEstDual = xcpxEstDual(nrNodes:end); % normalized
  % build poses
  posesEstDual = [real(pcpxEstDual) imag(pcpxEstDual)  phase(rcpxEstDual)];
  otherRelaxSol.EigWlam = anchorAndCheckCost(posesEstDual, edges, nrNodes);
  posesEstDual = otherRelaxSol.EigWlam.poses;
else
  %% GET SUBOPTIMAL SOLUTION, when SZEP is false
  % Eigenvector of WcpxLam_r
  otherRelaxSol.EigWlam = estimateFromEigenvector(WcpxLam_r, edges, nrNodes, zeroMultiplicityPose2D, GNrefinement); 
end

% Eigenvector of the rotation submatrix of WcpxLam_r
otherRelaxSol.EigWLamRot = estimateFromEigenvector(WcpxLam_r(nrNodes:end,nrNodes:end), edges, nrNodes, zeroMultiplicityPose2D, GNrefinement);
% Rank 1 approx of SDP solution
otherRelaxSol.Rank1SDPrelax = rank1approxSDPrelax(XX, edges, nrNodes, GNrefinement);
% Eigenvector of Schur complement matrix getting only rotation and eliminating positions: WcpxLam_r(nrNodes:end,nrNodes:end) = Q - diag(lam)
L = WcpxLam_r(1:nrNodes-1,1:nrNodes-1);
S = WcpxLam_r(1:nrNodes-1,nrNodes:end);
Ss = WcpxLam_r(nrNodes:end,1:nrNodes-1);
invL_S = L \ S;
YY = WcpxLam_r(nrNodes:end,nrNodes:end) - Ss * invL_S;
otherRelaxSol.EigWLamRotSchur = estimateFromEigenvector(YY, edges, nrNodes, zeroMultiplicityPose2D, GNrefinement);
% Eigenvector of reduced PGO matrix
otherRelaxSol.EigW = estimateFromEigenvector(Wcpx_r, edges, nrNodes, zeroMultiplicityPose2D, GNrefinement);
% Eigenvector of rotation only portion of PGO matrix
otherRelaxSol.EigWRot = estimateFromEigenvector(Wcpx_r(nrNodes:end,nrNodes:end), edges, nrNodes, zeroMultiplicityPose2D, GNrefinement);
% Solution from null space WcpxLam_r
[otherRelaxSol.NullSpaceWcpxLam_r, XXviaDual] = estimateFromNullSpace(WcpxLam_r, edges, nrNodes, zeroMultiplicityPose2D, GNrefinement);
% Beppe's
otherRelaxSol.LinearObjective = estimateLinearObjective(WcpxLam_r, edges, nrNodes, zeroMultiplicityPose2D, GNrefinement);
% Mole 2D
otherRelaxSol.Mole2D = estimateMole2D(edges, nrNodes, GNrefinement);
% store matrix as well
otherRelaxSol.Wcpx_r = Wcpx_r;

%% COMPARE SOLUTIONS
if norm(XX - XXviaDual) > 0.1
  norm(XX - XXviaDual), norm(XX - XXviaDual,'inf'), warning('XX - XXviaDual')
end

if zeroMultiplicityPose2D == 1 % in this case the rank relax must be exact
  if isequalWithTol(otherRelaxSol.Rank1SDPrelax.pstar, otherRelaxSol.EigWlam.pstar, 1e-2, 1e-4) == 0
    zeroMultiplicityPose2D, otherRelaxSol.Rank1SDPrelax.pstar, otherRelaxSol.EigWlam.pstar
    warning('Strange behaviour of rank 1 solution')
  end
end

pstarViaDual = otherRelaxSol.EigWlam.pstar;
posesEstDual = otherRelaxSol.EigWlam.poses;
fprintf('pstarViaDual = %g (SZEP=%d) \n',pstarViaDual,zeroMultiplicityPose2D)

if zeroMultiplicityPose2D == 1
  xrealStar = pose2DMatToVectPosCosSin(posesEstDual);
  xcpx = realvec2complex(xrealStar);
  xcpx = xcpx(2:end); % removing position anchor
  if norm(WcpxLam_r * xcpx) > 1
    warning('strange: solution not in null space of WcpxLam_r when SZEP')
  end
end

%% DO VERIFICATION
if doV2 == 1
  %% Populate with statistics from V2
  [otherRelaxSol.EigWlam] = verification2(otherRelaxSol.EigWlam, Wcpx_r);
  [otherRelaxSol.EigWLamRot] = verification2(otherRelaxSol.EigWLamRot, Wcpx_r);
  [otherRelaxSol.Rank1SDPrelax] = verification2(otherRelaxSol.Rank1SDPrelax, Wcpx_r);
  [otherRelaxSol.EigWLamRotSchur] = verification2(otherRelaxSol.EigWLamRotSchur, Wcpx_r);
  [otherRelaxSol.EigWRot] = verification2(otherRelaxSol.EigWRot, Wcpx_r);
  [otherRelaxSol.NullSpaceWcpxLam_r] = verification2(otherRelaxSol.NullSpaceWcpxLam_r, Wcpx_r);
  [otherRelaxSol.LinearObjective] = verification2(otherRelaxSol.LinearObjective, Wcpx_r);
  [otherRelaxSol.Mole2D] = verification2(otherRelaxSol.Mole2D, Wcpx_r);
  [otherRelaxSol.GNfromGT] = verification2(otherRelaxSol.GNfromGT, Wcpx_r);
  [otherRelaxSol.GNfromOdom] = verification2(otherRelaxSol.GNfromOdom, Wcpx_r);
end