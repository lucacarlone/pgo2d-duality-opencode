function [EigWlamSol] = anchorAndCheckCost(posesEstDual, edges, nrNodes)

%% cost before anchoring
[pstarViaDual_preAnchoring, ~, ~] = evaluateFrobeniousCostPose2D(edges, posesEstDual); % cost after anchoring

%% Express wrt anchor
for i=1:nrNodes 
  if i==1
    anchor = posesEstDual(1,:)';
    posesEstDual(1,:) = zeros(1,3);
  else
    posesEstDual(i,:) = poseSubNoisy(posesEstDual(i,:)', anchor, 0 , 0)';
  end
end
EigWlamSol.poses = posesEstDual;

%% check cost
[pstarViaDual, c1, c2] = evaluateFrobeniousCostPose2D(edges, posesEstDual); % cost after anchoring
if isequalWithTol(pstarViaDual_preAnchoring, pstarViaDual, 1e-4, 1e-5) == 0
  pstarViaDual_preAnchoring, pstarViaDual, error('mismatch pstarEigWlamNormalized - pstarViaDual')
end

%% save anchored estimate
EigWlamSol.pstar = pstarViaDual;
