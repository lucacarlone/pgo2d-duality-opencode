clear all
close all
clc

tol = 1e-7;

%% Test 1:
load('test1-singleLoopFail-n5.mat','edges', 'thGT', 'posesGT', 'posesInit');
nodesToRemove = 5;
expectedEdges = ...
  [1.000000000000000   2.000000000000000   0.342850148589180   6.822036008233306   0.586575999537721   1 0 0 1 0 1
   2.000000000000000   3.000000000000000  -1.582034114661180   5.468402566184125  -0.933762946707950   1 0 0 1 0 1
   3.000000000000000   4.000000000000000  -5.363193879784265   1.548659734667680   0.000080508061151   1 0 0 1 0 1
   4.000000000000000   1.000000000000000   1.941985017602212   0.330830230453689  -3.036891004017128   1 0 0 1 0 1];
 
 expectedThGT = [ 0.626231366703949
   1.768238735427200
   1.998381942380798
  -1.739924713326992];

expectedPosesGT = [ 0                   0                   0
  -0.012268033358392   5.877839720221209   1.142007368723252
  -5.606216831808556   7.682524490914025   1.372150575676850
  -9.051199287219003   2.920041297960286  -2.366156080030941];

expectedposesInit = [ 0                   0                   0
   0.342850148589180   6.822036008233306   0.586575999537721
  -4.001562248975633  10.500668836565874  -0.347186947170229
  -8.517815983557517  13.781773493661035  -0.347106439109078];

expectedNrNodes  = 4;

[actualEdges, actualThGT, actualPosesGT, actualPosesInit, actualNrNodes] = removeNode(edges, thGT, posesGT, posesInit,nodesToRemove);

if norm(expectedEdges - actualEdges)>tol
  error('fail: expectedEdges - actualEdges')
end
if norm(expectedThGT - actualThGT)>tol
  error('fail: expectedThGT - actualEdges')
end
if norm(expectedPosesGT - actualPosesGT)>tol
  error('fail: expectedPosesGT - actualPosesGT')
end
if norm(expectedposesInit - actualPosesInit)>tol
  error('fail: expectedposesInit - actualPosesInit')
end
if norm(expectedNrNodes - actualNrNodes)>tol
  error('fail: expectedNrNodes - actualNrNodes')
end

%% Test2
load('test1-singleLoopFail-n5.mat','edges', 'thGT', 'posesGT', 'posesInit');
nodesToRemove = 4;
expectedEdges = ...
  [   1.000000000000000   2.000000000000000   0.342850148589180   6.822036008233306   0.586575999537721 1 0 0 1 0 1
   2.000000000000000   3.000000000000000  -1.582034114661180   5.468402566184125  -0.933762946707950 1 0 0 1 0 1
   3.000000000000000   4.000000000000000  -5.036066259706496   7.929701517150082  -0.762374689063597 1 0 0 1 0 1
   4.000000000000000   1.000000000000000   5.346257098220343  -3.260119848756223  -2.2744358068923801 1 0 0 1 0 1];
 
 [actualEdges] = removeNode(edges, thGT, posesGT, posesInit,nodesToRemove);
 if norm(expectedEdges - actualEdges)>tol
  error('fail: expectedEdges - actualEdges')
end
 
 