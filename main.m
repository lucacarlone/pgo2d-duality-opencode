% This code creates a small pose graph optimization (PGO), and
% 1) iteratively solves the primal problem using gauss newton (GN)
% 2) solve the dual (SDP)
% 3) compute the duality gap
%
% Author: Luca Carlone
% Date: 2015-1-1
% Institution: Georgia Institute of Technology
%
% Requirements:
% -- the code requires CVX to perform the optimization

clear all
close all
clc

addpath('./lib');
nrNodes = 10; % number of nodes (poses) in the graph (placed at random)
isUniform = 0; % if zero, measurement noise is gaussian, otherwise it is uniform
sigmaR = 0.1; % std of rotation noise
sigmaT = 0.1; % std of translation noise
pLC = 0.1; % probability of connecting two nodes (loop closure)
doPlot  = 1; % show plot
tol = 1e-6; % numerical tolerance
gtColor = 'g'; % color of ground truth pose graph

%% create pose graph
[edges, thGT, posesGT, posesInit] = buildRandomGraph(nrNodes, isUniform, sigmaR, sigmaT, pLC, 1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  SOLVE THE PRIMAL VIA ITERATIVE METHODS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Gauss-Newton from GT
poseGNfromGTVect = refinePose2D(edges, thGT, posesGT);
posesGNfromGT = pose2DvectToMat(poseGNfromGTVect);
% Evaluate cost 
[pstar, c1, c2] = evaluateFrobeniousCostPose2D(edges, posesGNfromGT);
fprintf('pstar = %g (candidate primal optimal value) \n',pstar)
if ~exist('doPlot','var') || (exist('doPlot','var') && doPlot == 1)
  Fig = figure(1); clf; axes('FontSize', 14); hold on
  plotGraphPose2D(edges, posesGT, gtColor, Fig); title('ground truth poses')
  Fig = figure(2); clf; axes('FontSize', 14); hold on
  plotGraphPose2D([], posesGNfromGT, 'm', Fig); title('candidate primal solution')
end
GNfromGT.pstar = pstar;
GNfromGT.poses = posesGNfromGT;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SOLVE DUAL PROBLEM IN COMPLEX DOMAIN 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% build matrices Wreal and Wcpx
[Wreal,Wcpx,Wcpx_r] = buildPGOmatricesAndCheck(edges, nrNodes,pstar, posesGNfromGT);

%% solve dual problem
disp('-- Solving cpx dual problem with position anchor')
[dstar_r, lambda_r, WcpxLam_r, timeSDP_r] = solveDualCpxPositionAnchor(Wcpx_r,nrNodes);

%% check duality gap
fprintf('dstar_r = %g \n',dstar_r)
dgap = pstar - dstar_r;
fprintf('dgap = %g \n',dgap)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTE STATISTICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Test eigenvalues/vector
% PGO matrix
[V_Wcpx,D_Wcpx,eigWcpx] = getEig(Wcpx);
% reduced PGO matrix
[V_Wcpx_r,D_Wcpx_r,eigWcpx_r] = getEig(Wcpx_r);
% reduced penalized PGO matrix
[V_WcpxLam_r,D_WcpxLam_r,eigWcpxLam_r] = getEig(WcpxLam_r);
% number of eigenvalues in zero for the penalized pgo matrix
zeroMultiplicityPose2D = length( find(eigWcpxLam_r < tol) );


