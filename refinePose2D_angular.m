function poseEst = refinePose2D_angular(edges, thEst, posesInit)

isDebug = 1;

m = size(edges,1);
nrNodes = length(thEst); % thEst is the orientation estimate

fhat = zeros(3*m,1);
cost = 1e+20; % initial cost set to infinite
% Initial guess: written as a vector, with xy positions first, then theta
if  nargin < 3
  poseEst = [zeros(2*nrNodes,1); thEst]; 
else
  poseEst = [reshape(posesInit(:,1:2)',2*nrNodes,1); posesInit(:,3)]; 
end

nonAnchorInds = [3:2*nrNodes,2*nrNodes+2:3*nrNodes];
JC = spalloc(3*m, 2*nrNodes, 6*3*m); % 6 nonzero elements per row
JR = spalloc(3*m, nrNodes, 6*3*m); % 6 nonzero elements per row

for iter=1:10 
  %% linerize
  costPrec = cost;
  for k=1:m  
    tic
    idi = edges(k,1);
    idj = edges(k,2);
    % Guess
    thetai = poseEst(2*nrNodes+idi);
    Ri = rot2D(thetai);
    thetaj = poseEst(2*nrNodes+idj);
    % measurement
    thetaij = edges(k,5);
    Deltaij = edges(k,3:4)';
    
    idic = blockToMatIndices(idi,2);
    pi = poseEst(idic);
    idjc = blockToMatIndices(idj,2);
    pj = poseEst(idjc);
 
    % [pj(1) - pi(1) - cos(thetai) * deltaij(1) + sin(thetai) * deltaij(2)
    % [pj(2) - pi(2) - sin(thetai) * deltaij(1) - cos(thetai) * deltaij(2)
    % wrapToPi(thetaj - thetai)
    fcart = pj - pi - Ri * Deltaij;
    frot  = wrapToPi(thetaj - thetai - thetaij);   
    
    %% here we do not need the 1/2, since is [cos sin] instead of rotation
    inf_th = sqrt ( edges(k,11) ); % inverse of std for rotation measurements 
    inf_cart = sqrt ( edges(k,6) ); % inverse of std for position measurements 
    
    Omegaij = [ edges(k,6) edges(k,7) edges(k,8);
                edges(k,7) edges(k,9) edges(k,10);
                edges(k,8) edges(k,10) edges(k,11)];
    sqrtOmegaij = chol(Omegaij);
       
    % each block measurement f is f=[fcart frot]
    rowInd = blockToMatIndices(k,3);
    fhat(rowInd) = sqrtOmegaij*[fcart; frot];
 
    JR(rowInd,idi) = sqrtOmegaij*[ (sin(thetai)*Deltaij(1) + cos(thetai)* Deltaij(2)); 
                                   (-cos(thetai)*Deltaij(1) + sin(thetai)* Deltaij(2));
                                    -1 ];
    JR(rowInd,idj) = sqrtOmegaij*[ zeros(2,1); 
                        +1 ]; 
     
    JC(rowInd,idic) = sqrtOmegaij*[-eye(2) ; 
                                    zeros(1,2)]; 
    JC(rowInd,idjc) = sqrtOmegaij*[+eye(2) ; 
                                     zeros(1,2)]; 
  end
  %% current cost
  cost = 0.5 * norm(fhat)^2;
  
  %% delete anchor
  Jtot = [JC(:,3:end) JR(:,2:end)]; % cartesian part first
  corrEst = - (Jtot'*Jtot) \ (Jtot' * fhat);% minus because of fhat
  
  %% update estimate
  poseEst(nonAnchorInds) = poseEst(nonAnchorInds) + corrEst; % update estimate, preserving anchors 
  
  %% check stopping condition
  relCostChange = abs((cost - costPrec)/costPrec);
  if (isDebug>0) 
    fprintf('Current cost: %f, norm of the correction: %f, relative decrease: %.10f \n', cost, norm(corrEst), relCostChange); 
  end   
  if norm(corrEst) < 1e-4 || relCostChange < 1e-5
    break;
  end
end

if (isDebug==2) 
  figure
  plot(poseEst(1:2:2*nrNodes),poseEst(2:2:2*nrNodes),'-k');
  title('Estimate from refinePose2D')
end

end