function poseEst = refinePose2D_angular_gtsam(edges, thEst, posesInit)

%% Math:
% log(inv(Tij) * inv(Ti) * Tj)
% log ([Rij'  - Rij' tij]  *  [Ri'  - Ri' ti]  * [Rj  tj])
%     ([0              1]     [0           1]    [0    1]) = 
%
% log ([Rij'  - Rij' tij]  *  [Ri' Rj   Ri' (tj - ti)])
%     ([0              1]     [0                    1]) =
%
% log ([Rij'Ri' Rj       Rij'(  Ri' (tj - ti) - tij) ] )
%     ([0                                            1])

%%
isDebug = 1;

m = size(edges,1);
nrNodes = length(thEst); % thEst is the orientation estimate

fhat = zeros(3*m,1);
cost = 1e+20; % initial cost set to infinite
% Initial guess: written as a vector, with xy positions first, then theta
if  nargin < 3
  poseEst = [zeros(2*nrNodes,1); thEst]; 
else
  poseEst = [reshape(posesInit(:,1:2)',2*nrNodes,1); posesInit(:,3)]; 
end

nonAnchorInds = [3:2*nrNodes,2*nrNodes+2:3*nrNodes];
JC = spalloc(3*m, 2*nrNodes, 6*3*m); % 6 nonzero elements per row
JR = spalloc(3*m, nrNodes, 6*3*m); % 6 nonzero elements per row

for iter=1:10 
  %% linerize
  costPrec = cost;
  for k=1:m  
    tic
    idi = edges(k,1);
    idj = edges(k,2);
    % Guess
    thetai = poseEst(2*nrNodes+idi);
    Ri = rot2D(thetai);
    thetaj = poseEst(2*nrNodes+idj);
    Rj = rot2D(thetaj);
    % measurement
    thetaij = edges(k,5);
    Rij = rot2D(thetaij);
    Deltaij = edges(k,3:4)';
    
    idic = blockToMatIndices(idi,2);
    pi = poseEst(idic);
    idjc = blockToMatIndices(idj,2);
    pj = poseEst(idjc);

    fcart = Rij' * (  Ri' * (pj - pi) - Deltaij);
    frot  = wrapToPi(thetaj - thetai - thetaij);   
    
    %% here we do not need the 1/2, since is [cos sin] instead of rotation
    Omegaij = [ edges(k,6) edges(k,7) edges(k,8);
                edges(k,7) edges(k,9) edges(k,10);
                edges(k,8) edges(k,10) edges(k,11)];
    sqrtOmegaij = chol(Omegaij);
       
    % each block measurement f is f=[fcart frot]
    rowInd = blockToMatIndices(k,3);
    fhat(rowInd) = sqrtOmegaij*[fcart; frot];
 
    % Rij' * (  Ri' * (pj + Rj*(deltaj) - pi - Ri*(deltai)) - Deltaij);
    JR(rowInd,idi) = sqrtOmegaij*[ Rij' * ([-sin(thetai)  -cos(thetai); cos(thetai)  -sin(thetai)]' * (pj - pi) );
                                    -1 ];
    JR(rowInd,idj) = sqrtOmegaij*[ zeros(2,1); 
                        +1 ]; 
     
    JC(rowInd,idic) = sqrtOmegaij*[-Rij'  ; 
                                    zeros(1,2)]; 
    JC(rowInd,idjc) = sqrtOmegaij*[+Rij' *  Ri' * Rj; 
                                     zeros(1,2)];
        
     %% GTSAM replica:
     Ti = [Ri pi; 0 0 1];
     Tj = [Rj pj; 0 0 1];
     %  ==  Lie/Between -> Lie/between
     inv_between_Ti_Tj = inv(inv(Ti)*Tj);
     c = inv_between_Ti_Tj(1,1);
     s = inv_between_Ti_Tj(2,1);
     x = inv_between_Ti_Tj(1,3);
     y = inv_between_Ti_Tj(2,3);
     AdjointMap =[ c,  -s,     y;
         s,   c,  -x;
         0.0, 0.0, 1.0];
     H1 = - AdjointMap;
     % H2 = eye(3,3);
     Jpose1gtsam = sqrtOmegaij*[Rij' * Ri' * Rj    zeros(2,1);
         zeros(1,2)              1] * H1;
     Jpose2gtsam = sqrtOmegaij*[Rij' * Ri' * Rj    zeros(2,1);
         zeros(1,2)              1];

     %% repackage of my jacobians
     Jpose1 = full([JC(rowInd,idic)  JR(rowInd,idi)]);
     Jpose2 = full([JC(rowInd,idjc)  JR(rowInd,idj)]);
     if norm(Jpose1-Jpose1gtsam)>1e-5
         Jpose1; Jpose1gtsam
         error('mismatch1');
     end
     if norm(Jpose2-Jpose2gtsam)>1e-5
         Jpose2; Jpose2gtsam
         error('mismatch2');
     end
  end 
 
  %% current cost
  cost = 0.5 * norm(fhat)^2;
  
  %% delete anchor
  Jtot = [JC(:,3:end) JR(:,2:end)]; % cartesian part first
  corrEst = - (Jtot'*Jtot) \ (Jtot' * fhat);% minus because of fhat
  
  %% update estimate
  for idi=2:nrNodes
      thetai = poseEst(2*nrNodes+idi);
      Ri = rot2D(thetai);
      idic = blockToMatIndices(idi-1,2);
      corrEst(idic) = Ri * corrEst(idic);
  end
  poseEst(nonAnchorInds) = poseEst(nonAnchorInds) + corrEst; % update estimate, preserving anchors 
  
  %% check stopping condition
  relCostChange = abs((cost - costPrec)/costPrec);
  if (isDebug>0) 
    fprintf('Current cost: %f, norm of the correction: %f, relative decrease: %.10f \n', cost, norm(corrEst), relCostChange); 
  end   
  if norm(corrEst) < 1e-4 || relCostChange < 1e-5
    break;
  end
end

if (isDebug==2) 
  figure
  plot(poseEst(1:2:2*nrNodes),poseEst(2:2:2*nrNodes),'-k');
  title('Estimate from refinePose2D')
end

end